package training;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DB {

	public static void main(String[] args) {

		String url = "jdbc:mysql://localhost:3306/demo?useSSL=false";
		String user = "root";
		String password = "root";

//        String author = "Trygve Gulbranssen";
//        String sql = "INSERT INTO demo.students(name) VALUES(?)";
		String sql = "SELECT VERSION()";
		try (Connection con = DriverManager.getConnection(url, user, password);
				Statement st = con.createStatement();
				ResultSet rs = st.executeQuery(sql)) {

			if (rs.next()) {

				System.out.println(rs.getString(1));
			}

		} catch (SQLException ex) {

			Logger lgr = Logger.getLogger(DB.class.getName());
			lgr.log(Level.SEVERE, ex.getMessage(), ex);
		}
//        try (Connection con = DriverManager.getConnection(url, user, password);
//                PreparedStatement pst = con.prepareStatement(sql)) {
//
//            pst.setString(1, author);
//            pst.executeUpdate();
//            
//            System.out.println("A new author has been inserted");
//
//        } catch (SQLException ex) {
//
//            Logger lgr = Logger.getLogger(DB.class.getName());
//            lgr.log(Level.SEVERE, ex.getMessage(), ex);

		System.out.println("1- Add Student");
		System.out.println("2- Update Student");
		System.out.println("3- Delete Student");
		System.out.println("4- Show All Students");
		System.out.println("5- Quit");
		System.out.println("=======================");
		System.out.println("Please Enter your Choice");

		Scanner user1 = new Scanner(System.in); // Create a Scanner object
		int n = user1.nextInt();
		;
		if (n == 1) {

		}
		if (n == 2) {

		}
		if (n == 3) {

		}
		if (n == 4) {

		}
		if (n == 5) {

		}

//
//        }
	}
}